// @flow

import * as React from 'react';
import withCounter from './screens/Counter/withCounter';
import CounterComponent from './screens/Counter/CounterComponent';
import withColor from './screens/Counter/withColor';

type Props = {
  counter: number,
  onIncrement: () => void,
  onDecrement: () => void,
  withColorProps: Object,
};

function App(props: Props) {
  const {counter, onIncrement, onDecrement, withColorProps} = props;
  return (
    <CounterComponent
      style={{
        backgroundColor: withColorProps.color,
      }}
      counter={counter}
      onIncrement={() => {
        onIncrement();
        withColorProps.changeColor();
      }}
      onDecrement={() => {
        onDecrement();
        withColorProps.changeColor();
      }}
    />
  );
}

export default withColor(withCounter(App));
