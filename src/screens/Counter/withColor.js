import React, {PureComponent} from 'react';

const generateColor = () => {
  return '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6);
};

const withColor = (EnhancedComponent) => {
  return class WithColors extends PureComponent {
    state = {
      color: generateColor(),
    };

    changeColor = () => {
      this.setState({
        color: generateColor(),
      });
    };

    render() {
      const {color} = this.state;
      return (
        <EnhancedComponent
          withColorProps={{
            color: color,
            changeColor: this.changeColor,
          }}
        />
      );
    }
  };
};

export default withColor;
